#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX_SPECIES 5

// antoine constant indices
#define A 0
#define B 1
#define C 2
// thermochemical data indices
#define T_C 0
#define P_C 1
#define V_C 2
#define Z_C 3
#define OMEGA 4
#define V_M 5
// problem types
#define BUBBLE_T 3
#define DEW_T 4
#define BUBBLE_P 1
#define DEW_P 2
#define FLASH 5

double ANTOINE[MAX_SPECIES][3]; // antoine constants
double WILSON[MAX_SPECIES][MAX_SPECIES]; // wilson coefficients
double THERMO[MAX_SPECIES][6]; // thermophysical data
int speciesList[MAX_SPECIES]; // to store the list of species indices
int numSpecies = 0; // number of species

char *SPECIES_NAMES[MAX_SPECIES] = {"Acetone", "Methanol", "Water", "Methylacetate", "Benzene"};


double pSat(int which, double T) {
    double *antCoeffs = ANTOINE[which];
    double p = exp(antCoeffs[A] - antCoeffs[B] / (T + antCoeffs[C]));
    return p; // kPa
}
double tSat(int which, double P) {
    double *antCoeffs = ANTOINE[which];
    double t = antCoeffs[B] / (antCoeffs[A] - log(P)) - antCoeffs[C];
    return t; // degree C
}

double lambda(int i, int j, double T) {
    // molar volumes
    double Vi = THERMO[i][V_M];
    double Vj = THERMO[j][V_M];

    // wilson coefficient corresponding to ith and jth species
    double aij = WILSON[i][j];

    // wilson parameters are given in cal / mol
    double R = 1.9872; // cal / mol / K
    double lambda = (Vj / Vi) * exp(-aij / R / (T + 273.15));

    return lambda;
}
double gamma(int i, double T, double x[]) {
    double sum1 = 0;
    double sum2 = 0;

    // first sum inside ln()
    for (int sj = 0; sj < numSpecies; ++sj) {
        int j = speciesList[sj];
        // lambda = 1 when i = j
        if (i == j) {
            sum1 += x[j] * 1;
        } else {
            sum1 += x[j] * lambda(i, j, T);
        }
    }

    for (int sk = 0; sk < numSpecies; ++sk) {
        int k = speciesList[sk];
        // calculate the sum on the denominator first
        double bottomSum = 0;
        for (int sj = 0; sj < numSpecies; ++sj) {
            int j = speciesList[sj];
            bottomSum += x[j] * lambda(k, j, T);
        }

        // using the sum on the denominator, calculate the fraction in second sum
        sum2 += x[k] * lambda(k, i, T) / bottomSum;
    }

    // activity coefficient using wilson equation
    double gamma = exp(1 - log(sum1) - sum2);
    return gamma;
}
double b(int i, int j, double T) {
    // arithmetic average of critical compressibility factors
    double Zc = 0.5 * (THERMO[i][Z_C] + THERMO[j][Z_C]);

    // geometric average of critical temperatures
    double Tc = sqrt(THERMO[i][T_C] * THERMO[j][T_C]);

    // cube of average of cuberoots of critical volumes
    double Vc = pow(
            0.5 * (
                    pow(THERMO[i][V_C], 1.0 / 3)
                    + pow(THERMO[j][V_C], 1.0 / 3)
            ), 3);

    // find critical pressure using ideal gas law
    // since critical pressures are in bar, and critical volumes are in cm3 / mol
    // R is in bar x cm3 / mol / K
    double R = 83.14; // bar x cm3 / mol / K
    double Pc = Zc * R * Tc / Vc;

    // average of accentric factors
    double omegaIJ = 0.5 * (THERMO[i][OMEGA] + THERMO[j][OMEGA]);

    // convert T from C to K, and find reduced temperature
    double Tr = (T + 273.15) / Tc;

    // find second virial coefficients from general correlation
    double b0 = 0.083 - 0.422 / pow(Tr, 1.6);
    double b1 = 0.139 - 0.172 / pow(Tr, 4.2);
    double bHat = (b0 + omegaIJ * b1);
    double bij = bHat * R * Tc / Pc;

    return bij;
}
double phi(int i, double T, double P, double y[]) {

    // the big sum on the nominator in equation 14.6
    double sumsum = 0;
    for (int sj = 0; sj < numSpecies; ++sj) {
        int j = speciesList[sj];

        for (int sk = 0; sk < numSpecies; ++sk) {
            int k = speciesList[sk];
            // delta values
            double dji = 2 * b(j, i, T) - b(j, j, T) - b(i, i, T);
            double djk = 2 * b(j, k, T) - b(j, j, T) - b(k, k, T);

            sumsum += y[j] * y[k] * (2 * dji - djk);
        }
    }


    double Bii = b(i, i, T);
    double PiSat = pSat(i, T);

    double nom = Bii * (P - PiSat) + 0.5 * P * sumsum;
    // nominator is in units of saturation pressure, R must be in kPa x cm3 / mol / K
    double R = 8314; // kPa x cm3 / mol / K
    double den = R * T;
    double phi = exp(nom / den);

    return phi;
}

double calcTBubble(double P, double x[], double y[]) {
    double tBubble = 0;
    double tSatList[MAX_SPECIES] = {0};
    double pSatList[MAX_SPECIES] = {0};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    // calculate initial guess for bubble temperature
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        tSatList[i] = tSat(i, P);
        tBubble += x[i] * tSatList[i];
    }

    // using this temperature, find saturation pressures and gammas
    // and pressure
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pSatList[i] = pSat(i, tBubble);
        // gammas are 1
        // gammaList[i] = gamma(i, tBubble, x);
    }


    // pick j as the first species specified in the input
    int j = speciesList[0];
    double pjsum = 0;

    // eqn 14.13
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phis and gammas are 1
        pjsum += (x[i] * gammaList[i] / phiList[i]) * (pSatList[i] / pSatList[j]);
    }
    double pJSat = P / pjsum;

    // using pJSat find new guess for bubble temperature
    tBubble = tSat(j, pJSat);

    double error = 1;
    while (error > 1e-4) {
        // calculate fresh saturation pressures from old bubble temperature
        // also calculate fresh ys using these pressures
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            pSatList[i] = pSat(i, tBubble);

            // phis and gammas are 1
            y[i] = x[i] * gammaList[i] * pSatList[i] / (phiList[i] * P);
        }

        // eqn 14.13
        pjsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            // phis are 1
            // gammas are 1
            pjsum += (x[i] * gammaList[i] / phiList[i]) * (pSatList[i] / pSatList[j]);
        }
        pJSat = P / pjsum;

        // calculate new bubble temperature by eq. 14.15
        double tBubbleNew = tSat(j, pJSat);;

        error = fabs(tBubble - tBubbleNew);
        tBubble = tBubbleNew;
    }


    return tBubble;

}
double calcTBubbleModRaoults(double P, double x[], double y[]) {
    double tBubble = 0;
    double tSatList[MAX_SPECIES] = {0};
    double pSatList[MAX_SPECIES] = {0};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    // calculate initial guess for bubble temperature
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        tSatList[i] = tSat(i, P);
        tBubble += x[i] * tSatList[i];
    }

    // using this temperature, find saturation pressures and gammas
    // and pressure
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pSatList[i] = pSat(i, tBubble);
        gammaList[i] = gamma(i, tBubble, x);
    }


    // pick j as the first species specified in the input
    int j = speciesList[0];
    double pjsum = 0;

    // eqn 14.13
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phis are 1
        pjsum += (x[i] * gammaList[i] / phiList[i]) * (pSatList[i] / pSatList[j]);
    }
    double pJSat = P / pjsum;

    // using pJSat find new guess for bubble temperature
    tBubble = tSat(j, pJSat);

    double error = 1;
    while (error > 1e-4) {
        // calculate fresh saturation pressures from old bubble temperature
        // also calculate fresh ys using these pressures
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            pSatList[i] = pSat(i, tBubble);

            // phis are 1
            y[i] = x[i] * gammaList[i] * pSatList[i] / (phiList[i] * P);
        }

        // calculate fresh phis using fresh ys,
        // and fresh gammas at fresh temperatures
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];

            gammaList[i] = gamma(i, tBubble, x);

            // phis are 1
            // phiList[i] = phi(i, tBubble, P, y);
        }

        // eqn 14.13
        pjsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            // phis are 1
            pjsum += (x[i] * gammaList[i] / phiList[i]) * (pSatList[i] / pSatList[j]);
        }
        pJSat = P / pjsum;

        // calculate new bubble temperature by eq. 14.15
        double tBubbleNew = tSat(j, pJSat);;

        error = fabs(tBubble - tBubbleNew);
        tBubble = tBubbleNew;
    }


    return tBubble;
}
double calcTBubbleGammaPhi(double P, double x[], double y[]) {
    double tBubble = 0;
    double tSatList[MAX_SPECIES] = {0};
    double pSatList[MAX_SPECIES] = {0};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    // calculate initial guess for bubble temperature
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        tSatList[i] = tSat(i, P);
        tBubble += x[i] * tSatList[i];
    }

    // using this temperature, find saturation pressures and gammas
    // and pressure
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pSatList[i] = pSat(i, tBubble);
        gammaList[i] = gamma(i, tBubble, x);
    }


    // pick j as the first species specified in the input
    int j = speciesList[0];
    double pjsum = 0;

    // eqn 14.13
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pjsum += (x[i] * gammaList[i] / phiList[i]) * (pSatList[i] / pSatList[j]);
    }
    double pJSat = P / pjsum;

    // using pJSat find new guess for bubble temperature
    tBubble = tSat(j, pJSat);

    double error = 1;
    while (error > 1e-4) {
        // calculate fresh saturation pressures from old bubble temperature
        // also calculate fresh ys using these pressures
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            pSatList[i] = pSat(i, tBubble);

            y[i] = x[i] * gammaList[i] * pSatList[i] / phiList[i] / P;
        }

        // calculate fresh phis using fresh ys,
        // and fresh gammas at fresh temperatures
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            phiList[i] = phi(i, tBubble, P, y);
            gammaList[i] = gamma(i, tBubble, x);
        }

        // eqn 14.13
        pjsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];

            pjsum += (x[i] * gammaList[i] / phiList[i]) * (pSatList[i] / pSatList[j]);
        }
        pJSat = P / pjsum;

        // calculate new bubble temperature by eq. 14.15
        double tBubbleNew = tSat(j, pJSat);;

        error = fabs(tBubble - tBubbleNew);
        tBubble = tBubbleNew;
    }


    return tBubble;
}

double calcTDew(double P, double y[], double x[]) {
    double tDew = 0;
    double tSatList[MAX_SPECIES] = {0};
    double pSatList[MAX_SPECIES] = {0};

    // calculate saturation temperatures
    // and find an initial guess for the dew temperature
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        tSatList[i] = tSat(i, P);
        tDew += y[i] * tSatList[i];
    }

    // using this initial guess, calculate saturation pressures
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, tDew);
    }

    // pick j as the first species
    int j = speciesList[0];
    // and calculate pjsat using eq 14.14
    double jsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phi is 1 for modified raoults
        // gamma is 1 for modified raoults
        jsum += y[i] * 1 / 1 * (pSatList[j] / pSatList[i]);
    }
    double pJSat = P * jsum;

    tDew = tSat(j, pJSat);


    // using this new guess, calculate saturation pressures again
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, tDew);
    }

    // now calculate fresh xs
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phi is 1 for modified raoults
        // gamma is 1 for modified raoults
        x[i] = y[i] * P / 1 / pSatList[i];
    }

    double error = 1;
    while (error > 1e-4) {

        // using this new guess, calculate fresh saturation pressures
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            pSatList[i] = pSat(i, tDew);
        }


        // and calculate pjsat using eq 14.14
        jsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            // phi is 1 for modified raoults
            // gamma is 1 for modified raoults
            jsum += y[i] * 1 / 1 * (pSatList[j] / pSatList[i]);
        }
        pJSat = P * jsum;

        double tDewNew = tSat(j, pJSat);

        error = fabs(tDew - tDewNew);
        tDew = tDewNew;
    }

    return tDew;

}
double calcTDewModRaoults(double P, double y[], double x[]) {
    double tSatList[MAX_SPECIES] = {0};
    double pSatList[MAX_SPECIES] = {0};
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    // calculate saturation temperatures
    // and find an initial guess for the dew temperature
    double tDew = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        tSatList[i] = tSat(i, P);
        tDew += y[i] * tSatList[i];
    }

    // using this initial guess, calculate saturation pressures
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, tDew);
    }

    // pick j as the first species
    int j = speciesList[0];
    // and calculate pjsat using eq 14.14
    double jsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        jsum += y[i] * phiList[i] / gammaList[i] * (pSatList[j] / pSatList[i]);
    }
    double pJSat = P * jsum;

    tDew = tSat(j, pJSat);


    // using this new guess, calculate saturation pressures and phis again
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, tDew);

        // phis are 1 for modified raoults
        // phiList[i] = phi(i, tDew, P, y);
    }

    // now calculate fresh xs
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        x[i] = y[i] * phiList[i] * P / (gammaList[i] * pSatList[i]);
    }

    // xs are now known, calculate fresh gammas
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        gammaList[i] = gamma(i, tDew, x);
    }


    // and calculate pjsat using eq 14.14
    jsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        jsum += y[i] * phiList[i] / gammaList[i] * (pSatList[j] / pSatList[i]);
    }
    pJSat = P * jsum;

    tDew = tSat(j, pJSat);

    double error = 1;
    while (error > 1e-4) {

        // using this new guess, calculate fresh saturation pressures and fresh phis
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            pSatList[i] = pSat(i, tDew);

            // phis are 1 for modified raoults
            // phiList[i] = phi(i, tDew, P, y);
        }



        // now, x and gamma iteration
        double totalGammaError = 1;
        while (totalGammaError > 1e-4) {
            totalGammaError = 0;
            // calculate fresh xs using fresh phis
            double xsum = 0;
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                // phi is 1 for modified raoults
                x[i] = y[i] * phiList[i] * P / gammaList[i] / pSatList[i];
                xsum += x[i];
            }
            // normalize x values
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                x[i] = x[i] / xsum;
            }

            // using normalized x values, calculate fresh gammas
            // compare old and new gammas
            // if total gamma error is below minimum threshold,
            // then each gamma must be below max error, and iteration stops
            double gammaListNew[MAX_SPECIES] = {0};
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                // calculate new gammas
                gammaListNew[i] = gamma(i, tDew, x);
                // add up errors
                totalGammaError += fabs(gammaListNew[i] - gammaList[i]);
                // update gammas for next iteration
                gammaList[i] = gammaListNew[i];
            }
        }

        // and calculate pjsat using eq 14.14
        jsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            // phi is 1 for modified raoults
            jsum += y[i] * phiList[i] / gammaList[i] * (pSatList[j] / pSatList[i]);
        }
        pJSat = P * jsum;

        double tDewNew = tSat(j, pJSat);

        error = fabs(tDew - tDewNew);
        tDew = tDewNew;
    }

    return tDew;

}
double calcTDewGammaPhi(double P, double y[], double x[]) {
    double tSatList[MAX_SPECIES] = {0};
    double pSatList[MAX_SPECIES] = {0};
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    // calculate saturation temperatures
    // and find an initial guess for the dew temperature
    double tDew = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        tSatList[i] = tSat(i, P);
        tDew += y[i] * tSatList[i];
    }

    // using this initial guess, calculate saturation pressures
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, tDew);
    }

    // pick j as the first species
    int j = speciesList[0];
    // and calculate pjsat using eq 14.14
    double jsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        jsum += y[i] * phiList[i] / gammaList[i] * (pSatList[j] / pSatList[i]);
    }
    double pJSat = P * jsum;

    tDew = tSat(j, pJSat);


    // using this new guess, calculate saturation pressures and phis again
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, tDew);
        phiList[i] = phi(i, tDew, P, y);
    }

    // now calculate fresh xs
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        x[i] = y[i] * phiList[i] * P / (gammaList[i] * pSatList[i]);
    }

    // xs are now known, calculate fresh gammas
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        gammaList[i] = gamma(i, tDew, x);
    }


    // and calculate pjsat using eq 14.14
    jsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        jsum += y[i] * phiList[i] / gammaList[i] * (pSatList[j] / pSatList[i]);
    }
    pJSat = P * jsum;

    tDew = tSat(j, pJSat);

    double error = 1;
    while (error > 1e-4) {

        // using this new dew t guess,
        // calculate fresh saturation pressures and fresh phis
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            pSatList[i] = pSat(i, tDew);
            phiList[i] = phi(i, tDew, P, y);
        }



        // now, x and gamma iteration
        double totalGammaError = 1;
        while (totalGammaError > 1e-4) {
            totalGammaError = 0;

            // calculate fresh xs using fresh phis
            double xsum = 0;
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                x[i] = y[i] * phiList[i] * P / gammaList[i] / pSatList[i];
                xsum += x[i];
            }
            // normalize x values
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                x[i] = x[i] / xsum;
            }

            // using normalized x values, calculate fresh gammas
            // compare old and new gammas
            // if total gamma error is below minimum threshold,
            // then each gamma must be below max error, and iteration stops
            double gammaListNew[MAX_SPECIES] = {0};
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                // calculate new gammas
                gammaListNew[i] = gamma(i, tDew, x);
                // add up errors
                totalGammaError += fabs(gammaListNew[i] - gammaList[i]);
                // update gammas for next iteration
                gammaList[i] = gammaListNew[i];
            }
        }

        // and calculate pjsat using eq 14.14
        jsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            jsum += y[i] * phiList[i] / gammaList[i] * (pSatList[j] / pSatList[i]);
        }
        pJSat = P * jsum;

        double tDewNew = tSat(j, pJSat);

        error = fabs(tDew - tDewNew);
        tDew = tDewNew;
    }

    return tDew;
}

double calcPBubble(double T, double x[], double y[]) {
    double pBubble = 0;
    double pSatList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    // According to Raoults law,
    // bubble pressure is the weighted sum of saturation pressures
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pSatList[i] = pSat(i, T);
        pBubble += x[i] * pSatList[i];
    }

    // calculate ys
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phis and gammas are 1
        y[i] = x[i] * 1 * pSatList[i] / (1 * pBubble);
    }


    return pBubble;
}
double calcPBubbleModRaoults(double T, double x[], double y[]) {
    double pBubble = 0;
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double pSatList[MAX_SPECIES] = {0};

    for (int si = 0; si < numSpecies; ++si) {
        // get current species in the list given by user (not all available species)
        int i = speciesList[si];

        // calculate gammas and saturation pressures
        gammaList[i] = gamma(i, T, x);
        pSatList[i] = pSat(i, T);

        // add up and find bubble pressure
        pBubble += x[i] * gammaList[i] * pSatList[i];
    }

    // calculate ys using the old bubble pressure
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        y[i] = x[i] * gammaList[i] * pSatList[i] / pBubble;
    }


    return pBubble;
}
double calcPBubbleGammaPhi(double T, double x[], double y[]) {
    double pBubble = 0;
    double pSatList[MAX_SPECIES] = {0};

    // initial guess for gammas and phis
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    // find initial guess for bubble pressure
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pSatList[i] = pSat(i, T);
        gammaList[i] = gamma(i, T, x);

        pBubble += x[i] * gammaList[i] * pSatList[i] / phiList[i];
    }

    // iterate until bubble pressure converges
    double error = 1;
    while (error > 1e-4) {

        // calculate new ys using old phis and bubble pressure
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];

            y[i] = x[i] * gammaList[i] * pSatList[i] / phiList[i] / pBubble;
        }

        // calculate new phis using fresh ys
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            phiList[i] = phi(i, T, pBubble, y);
        }


        // using these fresh ys and phis, find new bubble pressure
        double pBubbleNew = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];

            pBubbleNew += x[i] * gammaList[i] * pSatList[i] / phiList[i];
        }

        // update error and bubble pressure for next iteration
        error = fabs(pBubble - pBubbleNew);
        pBubble = pBubbleNew;
    }


    return pBubble;
}

double calcPDew(double T, double y[], double x[]) {
    double pDew = 0;
    double pSatList[MAX_SPECIES] = {0};

    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pSatList[i] = pSat(i, T);
    }

    double pdewsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phi is 1 for raoults
        // gamma is 1 for raoults
        pdewsum += y[i] * 1 / 1 / pSatList[i];
    }
    pDew = 1 / pdewsum;

    // calculate x values
    double xsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phi is 1 for raoults
        // gamma is 1 for raoults
        x[i] = y[i] * 1 * pDew / 1 / pSatList[i];
        xsum += x[i];
    }
    // normalize x values
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        x[i] = x[i] / xsum;
    }

    return pDew;
}
double calcPDewModRaoults(double T, double y[], double x[]) {
    double pDew = 0;
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double pSatList[MAX_SPECIES] = {0};

    // calculate saturation pressures
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, T);
    }

    double pdewsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phi is 1 for mod raoults
        pdewsum += y[i] * 1 / gammaList[i] / pSatList[i];
    }
    pDew = 1 / pdewsum;

    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phi is 1 for mod raoults
        x[i] = y[i] * 1 * pDew / gammaList[i] / pSatList[i];
    }


    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        gammaList[i] = gamma(i, T, x);
    }

    pdewsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        // phi is 1 for mod raoults
        pdewsum += y[i] * 1 / gammaList[i] / pSatList[i];
    }
    pDew = 1 / pdewsum;


    double error = 1;
    while (error > 1e-4) {
        // now, x and gamma iteration
        double gammaTooBig = 1;
        while (gammaTooBig > 1) {
            // calculate fresh xs using fresh gammas
            double xsum = 0;
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                // phi is 1 for mod raoults
                x[i] = y[i] * 1 * pDew / gammaList[i] / pSatList[i];
                xsum += x[i];
            }
            // normalize x values
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                x[i] = x[i] / xsum;
            }

            // using normalized x values, calculate fresh gammas
            double newGammaList[MAX_SPECIES] = {0};
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                newGammaList[i] = gamma(i, T, x);
            }

            // compare old and new gammas
            // if total gamma error is below minimum threshold,
            // then each gamma must be below max error, and iteration stops
            double totalGammaError = 0;
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                // calculate new gammas
                newGammaList[i] = gamma(i, T, x);
                // add up errors
                totalGammaError = fabs(newGammaList[i] - gammaList[i]);
                // update gammas for next iteration
                gammaList[i] = newGammaList[i];
            }

            if (totalGammaError < 1e-4) {
                // stop iterating further
                gammaTooBig = 0;
            }
        }

        // now, xs and gammas converged
        // calculate new dew pressure
        pdewsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            // phi is 1 for mod raoults
            pdewsum += y[i] * 1 / gammaList[i] / pSatList[i];
        }
        double pDewNew = 1 / pdewsum;

        error = fabs(pDew - pDewNew);
        pDew = pDewNew;
    }

    return pDew;
}
double calcPDewGammaPhi(double T, double y[], double x[]) {
    double pDew = 0;
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double pSatList[MAX_SPECIES] = {0};

    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];

        pSatList[i] = pSat(i, T);
    }

    double pdewsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pdewsum += y[i] * phiList[i] / gammaList[i] / pSatList[i];
    }
    pDew = 1 / pdewsum;

    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        x[i] = y[i] * phiList[i] * pDew / gammaList[i] / pSatList[i];
    }


    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        gammaList[i] = gamma(i, T, x);
    }

    pdewsum = 0;
    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pdewsum += y[i] * phiList[i] / gammaList[i] / pSatList[i];
    }
    pDew = 1 / pdewsum;


    double error = 1;
    while (error > 1e-4) {
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            phiList[i] = phi(i, T, pDew, y);
        }

        // now, x and gamma iteration
        double gammaTooBig = 1;
        while (gammaTooBig > 1) {
            // calculate fresh xs using fresh phis
            double xsum = 0;
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                x[i] = y[i] * phiList[i] * pDew / gammaList[i] / pSatList[i];
                xsum += x[i];
            }
            // normalize x values
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                x[i] = x[i] / xsum;
            }

            // using normalized x values, calculate fresh gammas
            // compare old and new gammas
            // if total gamma error is below minimum threshold,
            // then each gamma must be below max error, and iteration stops
            double totalGammaError = 0;
            double newGammaList[MAX_SPECIES] = {0};
            for (int si = 0; si < numSpecies; ++si) {
                int i = speciesList[si];
                // calculate new gammas
                newGammaList[i] = gamma(i, T, x);
                // add up errors
                totalGammaError = fabs(newGammaList[i] - gammaList[i]);
                // update gammas for next iteration
                gammaList[i] = newGammaList[i];
            }

            if (totalGammaError < 1e-4) {
                // stop iterating further
                gammaTooBig = 0;
            }
        }

        // now, xs and gammas converged
        // calculate new dew pressure
        pdewsum = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            pdewsum += y[i] * phiList[i] / gammaList[i] / pSatList[i];
        }
        double pDewNew = 1 / pdewsum;

        error = fabs(pDew - pDewNew);
        pDew = pDewNew;
    }

    return pDew;
}

double calcFlash(double T, double P, double z[], double x[], double y[]) {
    double pSatList[MAX_SPECIES] = {0};
    double kList[MAX_SPECIES] = {0};


    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, T);
    }

    // initial guess for vapor fraction
    double v = 0.9999;

    double totalError = 1;
    while (totalError > 1e-6) {
        totalError = 0;

        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            // phi is 1 for modified raoults
            // gamma is 1 for modified raoults
            kList[i] = 1 * pSatList[i] / (1 * P);
        }

        // using Newton's method, calculate new vapor fraction
        double F = 0;
        double dFdV = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            F += z[i] * (kList[i] - 1) / (1 + v * (kList[i] - 1));
            dFdV += -z[i] * pow((kList[i] - 1), 2) / pow((1 + v * (kList[i] - 1)), 2);
        }
        double vNew = v - F / dFdV;
        totalError += fabs(v - vNew);
        v = vNew;

        // calculate xs, ys and their errors, and update them for the next iteration
        double xNew[MAX_SPECIES] = {0};
        double yNew[MAX_SPECIES] = {0};
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            xNew[i] = z[i] / (1 + v * (kList[i] - 1));
            yNew[i] = xNew[i] * kList[i];
            totalError += fabs(x[i] - xNew[i]);
            totalError += fabs(y[i] - yNew[i]);
            x[i] = xNew[i];
            y[i] = yNew[i];
        }

    }

    return v;
}
double calcFlashModRaoults(double T, double P, double z[], double x[], double y[]) {
    double pSatList[MAX_SPECIES] = {0};
    double kList[MAX_SPECIES] = {0};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};

    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, T);
    }

    // initial guess for vapor fraction
    // Must be between 0 and 1
    // When it's 0.5, it sometimes diverges
    double v = 0.9999;

    double totalError = 1;
    while (totalError > 1e-4) {
        totalError = 0;

        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            // phi is 1 for modified raoults
            kList[i] = gammaList[i] * pSatList[i] / (1.0 * P);
        }

        // using Newton's method, calculate new vapor fraction
        double F = 0;
        double dFdV = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            F += z[i] * (kList[i] - 1) / (1 + v * (kList[i] - 1));
            dFdV += -z[i] * pow((kList[i] - 1), 2) / pow((1 + v * (kList[i] - 1)), 2);
        }
        double vNew = v - F / dFdV;
        totalError += fabs(v - vNew);
        v = vNew;

        // calculate xs, ys and their errors, and update them for the next iteration
        double xNew[MAX_SPECIES] = {0};
        double yNew[MAX_SPECIES] = {0};
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            xNew[i] = z[i] / (1 + v * (kList[i] - 1));
            yNew[i] = xNew[i] * kList[i];
            totalError += fabs(x[i] - xNew[i]);
            totalError += fabs(y[i] - yNew[i]);
            x[i] = xNew[i];
            y[i] = yNew[i];
        }

        // calculate fresh gammas
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            gammaList[i] = gamma(i, T, x);
        }
    }

    return v;

}
double calcFlashGammaPhi(double T, double P, double z[], double x[], double y[]) {
    double pSatList[MAX_SPECIES] = {0};
    double kList[MAX_SPECIES] = {0};
    double phiList[MAX_SPECIES] = {1, 1, 1, 1, 1};
    double gammaList[MAX_SPECIES] = {1, 1, 1, 1, 1};


    for (int si = 0; si < numSpecies; ++si) {
        int i = speciesList[si];
        pSatList[i] = pSat(i, T);
    }

    // initial guess for vapor fraction
    // Must be between 0 and 1
    // When it's 0.5, it sometimes diverges
    double v = 0.9999;

    double totalError = 1;
    while (totalError > 1e-4) {
        totalError = 0;

        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            kList[i] = gammaList[i] * pSatList[i] / (phiList[i] * P);
        }

        // using Newton's method, calculate new vapor fraction
        double F = 0;
        double dFdV = 0;
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            F += z[i] * (kList[i] - 1) / (1 + v * (kList[i] - 1));
            dFdV += -z[i] * pow((kList[i] - 1), 2) / pow((1 + v * (kList[i] - 1)), 2);
        }
        double vNew = v - F / dFdV;
        totalError += fabs(v - vNew);
        v = vNew;

        // calculate xs, ys and their errors, and update them for the next iteration
        double xNew[MAX_SPECIES] = {0};
        double yNew[MAX_SPECIES] = {0};
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            xNew[i] = z[i] / (1 + v * (kList[i] - 1));
            yNew[i] = xNew[i] * kList[i];
            totalError += fabs(x[i] - xNew[i]);
            totalError += fabs(y[i] - yNew[i]);

            x[i] = xNew[i];
            y[i] = yNew[i];
        }

        // calculate fresh gammas and phis
        for (int si = 0; si < numSpecies; ++si) {
            int i = speciesList[si];
            gammaList[i] = gamma(i, T, x);
            phiList[i] = phi(i, T, P, y);
        }
    }

    return v;
}

void readData() {

    // =======================================
    // READ ANTOINE CONSTANTS
    // =======================================
    FILE *dataAntoine = fopen("data-antoine.txt", "r");

    if (dataAntoine == NULL) {
        printf("Can't find the Antoine constant table");
        printf("Specify the table of constants in 'data-antoine.txt'");
        exit(1);
    } else {
        // read antoine constants for all five species
        for (int si = 0; si < MAX_SPECIES; ++si) { // species index
            for (int ai = 0; ai < 3; ++ai) { // antoine constant index
                fscanf(dataAntoine, "%lf ", &ANTOINE[si][ai]);
            }
        }

        printf("Antoine constants are ready. \n");
    }
    fclose(dataAntoine);

    // =======================================
    // READ WILSON COEFFICIENTS
    // =======================================
    FILE *dataWilson = fopen("data-wilson.txt", "r");

    if (dataWilson == NULL) {
        printf("Can't find the Wilson coefficient table");
        printf("Specify the list of coefficients in 'data-wilson.txt'");
        exit(1);
    } else {
        for (int i = 0; i < MAX_SPECIES; ++i) {
            for (int j = 0; j < MAX_SPECIES; ++j) {
                fscanf(dataWilson, "%lf ", &WILSON[i][j]);
            }
        }

        printf("Wilson coefficients are ready. \n");
    }
    fclose(dataWilson);

    // =======================================
    // READ THERMOCHEMICAL DATA
    // =======================================
    FILE *dataThermo = fopen("data-thermochemical.txt", "r");


    if (dataThermo == NULL) {
        printf("Can't find the thermochemical data table");
        printf("Specify the table of constants in 'data-thermochemical.txt'");
        exit(1);
    } else {
        for (int i = 0; i < MAX_SPECIES; ++i) {
            for (int j = 0; j < 6; ++j) {
                fscanf(dataThermo, "%lf ", &THERMO[i][j]);
            }
        }

        printf("Thermochemical constants are ready. \n");
    }
    fclose(dataThermo);

    // =======================================
    // READ USER INPUTS
    // =======================================
    FILE *dataInputs = fopen("data-inputs.txt", "r");

    // check if input file exists
    if (dataInputs == NULL) {
        printf("Can't find the species list");
        printf("Specify the list of species in 'data-input.txt'");
        exit(1);
    } else {
        // Read and count the number of integers in the file
        while (fscanf(dataInputs, "%d ", &speciesList[numSpecies]) == 1) {
            numSpecies++;
        }

        printf("Species list is read successfully. \n");
        printf("\n");


        printf("Current species list and order: \n");
        for (int i = 0; i < numSpecies; ++i) {
            int si = speciesList[i];
            printf("%d. %s \n", (i + 1), SPECIES_NAMES[si]);
        }

        printf("\n\n");
    }
    fclose(dataThermo);

}
int main() {
    // Read data from files
    readData();


    int problemCount = 5;
    FILE *pr = fopen("data-problems.txt", "r");
    double problems[problemCount][6];

    for (int prob = 0; prob < problemCount; ++prob) {
        for (int param = 0; param < 6; ++param) {
            fscanf(pr, "%lf", &problems[prob][param]);
        }
    }


    // inputs from the user are read successfully

    for (int i = 0; i < problemCount; ++i) {
        double *problem = problems[i];

        int type = (int) problem[0];

        if (type == BUBBLE_T) {
            double P = problem[2];
            double xs[3] = {problem[3], problem[4], problem[5]};
            double ys[3] = {0};

            printf("BUBBLE TEMPERATURE\n");
            printf("P:\t%.1f kPa\n", P);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);
            printf("---------\n");

            double tb = calcTBubble(P, xs, ys);
            printf("R:\t%.3f C\n", tb);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);

            double tbMR = calcTBubbleModRaoults(P, xs, ys);
            printf("MR:\t%.3f C\n", tbMR);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);

            double tbGP = calcTBubbleGammaPhi(P, xs, ys);
            printf("GP:\t%.3f C\n", tbGP);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);

            printf("\n\n\n\n");
        } else if (type == DEW_T) {
            double P = problem[2];
            double ys[3] = {problem[3], problem[4], problem[5]};
            double xs[3] = {0};

            printf("DEW TEMPERATURE\n");
            printf("P:\t%.1f kPa\n", P);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);
            printf("---------\n");

            double td = calcTDew(P, ys, xs);
            printf("R:\t%.3f C\n", td);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);

            double tdMR = calcTDewModRaoults(P, ys, xs);
            printf("MR:\t%.3f C\n", tdMR);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);

            double tdGP = calcTDewGammaPhi(P, ys, xs);
            printf("GP:\t%.3f C\n", tdGP);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);

            printf("\n\n\n\n");
        } else if (type == BUBBLE_P) {
            double T = problem[1];
            double xs[3] = {problem[3], problem[4], problem[5]};
            double ys[3] = {0};

            printf("BUBBLE PRESSURE\n");
            printf("T:\t%.1f C\n", T);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);
            printf("---------\n");

            double pb = calcPBubble(T, xs, ys);
            printf("R:\t%.3f kPa\n", pb);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);

            double pbMR = calcPBubbleModRaoults(T, xs, ys);
            printf("MR:\t%.3f kPa\n", pbMR);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);

            double pbGP = calcPBubbleGammaPhi(T, xs, ys);
            printf("GP:\t%.3f kPa\n", pbGP);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);

            printf("\n\n\n\n");
        } else if (type == DEW_P) {
            double T = problem[1];
            double ys[3] = {problem[3], problem[4], problem[5]};
            double xs[3] = {0};

            printf("DEW PRESSURE\n");
            printf("T:\t%.1f C\n", T);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);
            printf("---------\n");

            double pd = calcPDew(T, ys, xs);
            printf("R:\t%.3f kPa\n", pd);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);

            double pdMR = calcPDewModRaoults(T, ys, xs);
            printf("MR:\t%.3f kPa\n", pdMR);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);

            double pdGP = calcPDewGammaPhi(T, ys, xs);
            printf("GP:\t%.3f kPa\n", pdGP);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);

            printf("\n\n\n\n");
        } else if (type == FLASH) {
            double P = problem[2];

            double zs[3] = {problem[3], problem[4], problem[5]};
            double xs[3] = {0};
            double ys[3] = {0};

            printf("FLASH\n");
            printf("P:\t%.1f kPa\n", P);
            printf("z:\t%.3f, %.3f, %.3f\n", zs[0], zs[1], zs[2]);
            printf("---------\n");

            double tb = calcTBubble(P, zs, ys);
            double td = calcTDew(P, zs, xs);
            double T = 0.5 * (tb + td);
            double v = calcFlash(T, P, zs, xs, ys);
            printf("T:\t(%.3f + %.3f)/2 = %.3f C \n", tb, td, T);
            printf("R:\t%.3f\n", v);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);
            printf("---\n");

            double tbMR = calcTBubbleModRaoults(P, zs, ys);
            double tdMR = calcTDewModRaoults(P, zs, xs);
            double TMR = 0.5 * (tbMR + tdMR);
            double vMR = calcFlashModRaoults(TMR, P, zs, xs, ys);
            printf("T:\t(%.3f + %.3f)/2 = %.3f C \n", tbMR, tdMR, TMR);
            printf("MR:\t%.3f\n", vMR);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);
            printf("---\n");

            double tbGP = calcTBubbleGammaPhi(P, zs, ys);
            double tdGP = calcTDewGammaPhi(P, zs, xs);
            double TGP = 0.5 * (tbGP + tdGP);
            double vGP = calcFlashGammaPhi(TGP, P, zs, xs, ys);
            printf("T:\t(%.3f + %.3f)/2 = %.3f C \n", tbGP, tdGP, TGP);
            printf("GP:\t%.3f\n", vGP);
            printf("x:\t%.3f, %.3f, %.3f\n", xs[0], xs[1], xs[2]);
            printf("y:\t%.3f, %.3f, %.3f\n", ys[0], ys[1], ys[2]);
            printf("\n\n\n\n");

        }
    }


    printf("Done.\n");
    printf("Press a key to exit\n");
//    getchar();
    return 0;
}

